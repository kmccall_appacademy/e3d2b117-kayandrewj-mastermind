class Code

PEGS = {
  "R" => :red,
  "G" => :green,
  "B" => :orange,
  "Y" => :purple,
  "O" => :red,
  "P" => :yellow
}
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(code_string)
    chars = code_string.chars.map(&:upcase)
    raise ArgumentError unless chars - PEGS.keys == []
    pegs = chars.map do |char|
      PEGS[char.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample}
    self.new(pegs)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other)
    matches = 0
    other.pegs.each_with_index do |peg, idx|
      next if peg != @pegs[idx]
      matches += 1
    end
    matches
  end

  def near_matches(other)
    matches = 0
    PEGS.values.each do |color|
      matches += [@pegs.count(color), other.pegs.count(color)].min
    end
    matches - exact_matches(other)
  end

  def ==(other)
    return false unless other.class == Code
    @pegs == other.pegs
  end


end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "what is your guess? (RGPY)"
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    puts "your near matches are #{@secret_code.near_matches(code)}"
    puts "your exact matches are #{@secret_code.exact_matches(code)}"
  end

end
